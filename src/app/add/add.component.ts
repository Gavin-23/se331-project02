import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../service/app-service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  name: string;
  location: string;
  description: string;
  period: any;
  host: string;
  students: string[]
  date: any;

  constructor(
    private appService: AppService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  add() {
    let record = {};
    record['name'] = this.name;
    record['location'] = this.location;
    record['description'] = this.description;
    record['period'] = this.period;
    record['host'] = this.host;
    record['date'] = this.date;
    record['students'] = [];

    this.appService.addActivity(record).then(resp => {
      this.name = "";
      this.location = "";
      this.description = "";
      this.period = "";
      this.date = "";
      this.host = "";
      this.students = [];
      this.router.navigateByUrl('/admin');

      console.log(resp);
      alert('Adding Successful!');
    })
      .catch(error => {
        console.log(error);
      });
  }


}
