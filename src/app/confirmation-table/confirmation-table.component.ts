import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';


import { BehaviorSubject } from 'rxjs';
import { ConfirmationTableDataSource } from './confirmation-table-datasource';
import { TeacherService } from '../service/teacher-service';
import { AppService } from '../service/app-service';
import { StudentService } from '../service/student-service';
import { Student } from '../entity/student';

@Component({
  selector: 'app-confirmation-table',
  templateUrl: './confirmation-table.component.html',
  styleUrls: ['./confirmation-table.component.css']
})
export class ConfirmationTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<any>;
  dataSource: ConfirmationTableDataSource;


  displayedColumns = ['activity', 'studentId', 'confirm', 'reject'];
  confirmation: any[];
  filter: string;
  filter$: BehaviorSubject<string>;
  seleted: any;
  name: string;
  location: string;
  description: string;
  date: any;
  period: any;
  host: string;
  students: string[]
  activities: any;
  editId: any;
  allStudents: any[];
  s_name: string;
  surname: string;
  image: string;
  birthday: any;
  studentId: string;
  email: string;
  id: string;
  password: string;
  s_activities: string[];
  constructor(private teacherService: TeacherService, private appService: AppService, private studentService: StudentService) {
  }
  ngOnInit() {

    this.teacherService.getConfirmation().subscribe(data => {
      this.confirmation = data.map(e => {
        return {
          activity: e.payload.doc.data()['activity'],
          studentId: e.payload.doc.data()['studentId'],
          id: e.payload.doc.id,
        };
      })

      this.dataSource = new ConfirmationTableDataSource();
      this.dataSource.data = this.confirmation;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;

    });

    this.appService.getActivities().subscribe(data => {
      this.activities = data.map(e => {
        return {
          date: e.payload.doc.data()['date'],
          description: e.payload.doc.data()['description'],
          host: e.payload.doc.data()['host'],
          location: e.payload.doc.data()['location'],
          name: e.payload.doc.data()['name'],
          period: e.payload.doc.data()['period'],
          students: e.payload.doc.data()['students'],
          id: e.payload.doc.id
        };
      })
    });


    this.appService.getStudents().subscribe(data => {
      this.allStudents = data.map(e => {
        return {
          name: e.payload.doc.data()['name'],
          surname: e.payload.doc.data()['surname'],
          birthday: e.payload.doc.data()['birthday'],
          email: e.payload.doc.data()['email'],
          image: e.payload.doc.data()['image'],
          password: e.payload.doc.data()['password'],
          activities: e.payload.doc.data()['activities'],
          studentId: e.payload.doc.data()['studentId'],
          id: e.payload.doc.id
        };
      })

    });

  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  save() {
    let record = {};
    record['name'] = this.name;
    record['location'] = this.location;
    record['description'] = this.description;
    record['period'] = this.period;
    record['host'] = this.host;
    record['date'] = this.date;
    record['students'] = this.students;



  }

  confirm(e) {
    this.teacherService.confirm(e.id);
    for (let i = 0; i < this.activities.length; i++) {
      if (this.activities[i].name == e.activity) {
        this.name = this.activities[i].name;
        this.location = this.activities[i].location;
        this.description = this.activities[i].description;
        this.period = this.activities[i].period;
        this.host = this.activities[i].host;
        this.date = this.activities[i].date;
        this.students = this.activities[i].students;
        this.editId = this.activities[i].id;
      }
    }

    let record = {};
    record['name'] = this.name;
    record['location'] = this.location;
    record['description'] = this.description;
    record['period'] = this.period;
    record['host'] = this.host;
    record['date'] = this.date;
    record['students'] = this.students;
    record['students'].push(e.studentId);

    this.teacherService.update(record, this.editId).then(resp => {
      this.name = "";
      this.location = "";
      this.description = "";
      this.period = "";
      this.date = "";
      this.host = "";
    }).catch(error => {
      console.log(error);
    });

    for (let i = 0; i < this.allStudents.length; i++) {
      if (this.allStudents[i].studentId == e.studentId) {
        this.s_name = this.allStudents[i].name;
        this.surname = this.allStudents[i].surname;
        this.birthday = this.allStudents[i].birthday;
        this.image = this.allStudents[i].image;
        this.s_activities = this.allStudents[i].activities;
        this.password = this.allStudents[i].password;
        this.email = this.allStudents[i].email;
        this.id = this.allStudents[i].id
        this.studentId = this.allStudents[i].studentId;
      }
    }
    let record2 = {};

    record2['name'] = this.s_name;
    record2['surname'] = this.surname;
    record2['image'] = this.image;
    record2['birthday'] = this.birthday;
    record2['studentId'] = this.studentId;
    record2['email'] = this.email;
    record2['password'] = this.password;
    record2['activities'] = this.s_activities;
    record2['activities'].push(e.id);

    console.log(record2);
    
    this.studentService.update(record2, this.id).then(resp => {
      
    })
      .catch(error => {
        console.log(error);
      });

  }

  reject(activity) {
    this.teacherService.confirm(activity);
  }
}
