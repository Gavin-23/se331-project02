import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyActivityTableComponent } from './my-activity-table.component';

describe('MyActivityTableComponent', () => {
  let component: MyActivityTableComponent;
  let fixture: ComponentFixture<MyActivityTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyActivityTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyActivityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
