import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../service/student-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: string;
  surname: string;
  image: string;
  birthday: any;
  studentId: any;
  email: string;
  password: string;
  activities: string[];

  constructor(
    private studentService: StudentService,
    private router: Router
  ) { }

  ngOnInit() {
  }


  registers() {
    let record = {};
    record['name'] = this.name;
    record['surname'] = this.surname;
    record['image'] = this.image;
    record['birthday'] = this.birthday;
    record['studentId'] = this.studentId;
    record['email'] = this.email;
    record['password'] = this.password;
    record['activities'] = [];

    this.studentService.register(record).then(resp => {
      this.name = "";
      this.surname = "";
      this.image = "";
      this.birthday = "";
      this.studentId = "";
      this.email = "";
      this.password = "";
      this.activities = [];
      this.router.navigateByUrl('/login');
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }
}
