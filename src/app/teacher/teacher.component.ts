import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  currentUser: string;
  constructor(private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('teacher') == null) {
      this.router.navigate(['/login']);
      alert('Please login');

    }
    this.currentUser = JSON.parse(localStorage.getItem('teacher'));
    
  }

}
