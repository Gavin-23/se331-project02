import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  currentUser: any;
  constructor(private router: Router) { 
    
  }

  ngOnInit() {
    if (localStorage.getItem('student') == null) {
      this.router.navigate(['/login']);
      alert('Please login');

    }
    this.currentUser = JSON.parse(localStorage.getItem('student'));
      
  }
  
}
